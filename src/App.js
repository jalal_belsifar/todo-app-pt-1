import React, { Component } from 'react';
import './index.css';
import todosList from './todos.json';
import TodoList from './TodoList';
import { Route, NavLink } from 'react-router-dom';

class App extends Component {
  state = {
    todos: todosList,
    value: '',
  };
  handleData = (event) => {
    if (event.key === 'Enter') {
      const newTodo = {
        userId: 1,
        id: Math.floor(Math.random() * 10000),
        title: this.state.value,
        completed: false,
      };
      const newItem = [...this.state.todos, newTodo];
      this.setState({ ...this.state, todos: newItem, value: '' });
    }
  };
  handleDataModified = (event) => {
    this.setState({ ...this.state, value: event.target.value });
  };

  handleCheckData = (id) => {
    const newCheck = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      }
      return todo;
    });
    this.setState((state) => {
      return {
        ...state,
        todos: newCheck,
      };
    });
  };
  handleSingleDelete = (event, id) => {
    const deleteCheck = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({ todos: deleteCheck });
  };

  handleDelteAllTodos = (event) => {
    const deleteAll = this.state.todos.filter(
      (todo) => todo.completed === false
    );
    this.setState({ todos: deleteAll });
  };

  render() {
    return (
      <section className='todoapp'>
        <header className='header'>
          <h1>todos</h1>
          <input
            className='new-todo'
            J
            placeholder='What needs to be done?'
            autoFocus
            value={this.state.value}
            onChange={this.handleDataModified}
            onKeyDown={this.handleData}
          />
        </header>
        <Route
          exact
          path='/'
          render={() => (
            <TodoList
              todos={this.state.todos}
              handleSingleDelete={this.handleSingleDelete}
              handleCheckData={this.handleCheckData}
            />
          )}
        />
        <Route
          path='/active'
          render={() => (
            <TodoList
              todos={this.state.todos.filter(
                (todo) => todo.completed === false
              )}
              handleCheckData={this.handleCheckData}
              handleSingleDelete={this.handleSingleDelete}
            />
          )}
        />

        <Route
          path='/completed'
          render={() => (
            <TodoList
              todos={this.state.todos.filter((todo) => todo.completed === true)}
              handleCheckData={this.handleCheckData}
              handleSingleDelete={this.handleSingleDelete}
            />
          )}
        />
        <footer className='footer'>
          <span className='todo-count'>
            <strong>
              {
                this.state.todos.filter((todo) => todo.completed !== true)
                  .length
              }
            </strong>
            item(s) left
          </span>
          <ul className='filters'>
            <li>
              <NavLink exact to='/' activeClassName='selected'>
                All
              </NavLink>
            </li>
            <li>
              <NavLink to='/active' activeClassName='selected'>
                Active
              </NavLink>
            </li>
            <li>
              <NavLink to='/completed' activeClassName='selected'>
                Completed
              </NavLink>
            </li>
          </ul>
          <button
            className='clear-completed'
            onClick={(event) => this.handleDelteAllTodos(event, this.props.id)}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

export default App;
