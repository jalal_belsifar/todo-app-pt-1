import React, { Component } from 'react';
import TodoItem from './TodoItem';

class TodoList extends Component {
  render() {
    return (
      <section className='main'>
        <ul className='todo-list'>
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              handleSingleDelete={(event) =>
                this.props.handleSingleDelete(event, todo.id)
              }
              title={todo.title}
              completed={todo.completed}
              handleCheckData={(event) => this.props.handleCheckData(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default TodoList;
